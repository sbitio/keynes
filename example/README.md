[[_TOC_]]
# Keynes example

Purpose of this directory is to show how to use [Keynes](https://gitlab.com/sbitio/keynes) library.

This directory contains:

 * Example of all Keynes features
 * Instructions to set up Jenkins with JobDSL and the Keynes library
 * A script to export all job definitions to a folder (see `tools/export-jobs`)

# Setup

## Manual setup of Jenkins to run a Keynes-based project

See [MANUAL_JENKINS_SETUP](./../MANUAL_JENKINS_SETUP.md) instructions.

## Docker

### Run the container

```bash
NAME=keynes-example
docker run --name $NAME \
  -p 8080:8080 \
  --volume $NAME:/var/jenkins_home \
  --mount type=bind,source="$(pwd)",target=/seed \
  registry.gitlab.com/sbitio/keynes:latest
```

### Cleanup

```bash
docker container stop $NAME
docker container rm $NAME
docker volume rm $NAME
```
