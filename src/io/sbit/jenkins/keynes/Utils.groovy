package io.sbit.jenkins.keynes

class Utils {
  /**
   * Log a message in the pipeline output.
   */
  @NonCPS
  private static void log(String message) {
    Seed.getInstance().steps.echo(message)
  }

  /**
   * Log a message in the pipeline output.
   */
  @NonCPS
  private static void warn(String message) {
    log "WARNING: ${message}"
  }

  /**
   * Log a message in the pipeline output.
   */
  @NonCPS
  private static void err(String message) {
    log "ERROR: ${message}"
    throw new Exception(message)
  }

  /**
   * Deep copy of ConfigObject.
   */
  @NonCPS
  private static def deepCopy(Map orig) {
    ConfigObject copy = new ConfigObject()
    orig.keySet().each { key ->
      def value = orig.get(key)
      if (value instanceof Map) {
        value = deepCopy(value)
      }
      copy.put(key, value)
    }
    return copy
  }
}
