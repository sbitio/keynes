package io.sbit.jenkins.keynes

import groovy.io.FileType
import org.yaml.snakeyaml.Yaml

class Project {
  String name
  String label
  String description
  Map authorization
  String path
  String folder
  ConfigObject config
  def pipelines
  private def steps

  Project(String name, String path, ConfigObject baseConfig) {
    this.name = name
    this.path = path
    this.steps = Seed.getInstance().steps

    String baseFolder = Seed.getInstance().baseFolder
    this.folder = baseFolder ? "${baseFolder}/${this.name}" : this.name

    // Config.
    this.config = mergeConfig(baseConfig, steps.readYamlConfig("$path/config.yml"))

    Yaml yaml = new Yaml()
    File file = new File("$path/config_merged.yml")
    file.write(yaml.dump(this.config))

    this.label = this.config.name
    this.description = this.config.description
    this.authorization = this.config.authorization

    this.pipelines = processPipelines()
  }

  /**
   * Returns a ConfigObject with project default configuration.
   */
  @NonCPS
  private ConfigObject defaultConfig() {
    new ConfigObject([
      'name'               : this.name,
      'description'        : '',
      'authorization'      : [:],
      'credentials'        : [:],
      'daysToKeep'         : -1,
      'numToKeep'          : 25,
      'artifactDaysToKeep' : -1,
      'artifactNumToKeep'  : 2,
      'timeout'            : 1440, // 24h in minutes
      'inventory_maps'     : [:],
      'files'              : [:],
      'pipelines'          : [:],
      // For ansible based pipelines.
      'ansible' : [
        'installation': '',
        'config'      : 'ansible/ansible.cfg',
        'inventory'   : 'ansible/inventory',
        'extra_vars'  : ['ansible/extra-vars.yml'],
        'extra_files' : [],
        'extras'      : '',
      ],
    ])
  }

  /**
   * Merge default, project and pipeline config.
   */
  @NonCPS
  private ConfigObject mergeConfig(ConfigObject baseConfig, ConfigObject projectConfig) {
    ConfigObject config = defaultConfig()

    config.merge(Utils.deepCopy(baseConfig))
    config.merge(projectConfig)

    Template t = new Template(config)

    return config
  }

  /**
   * Returns a map of pinelines.
   *
   * key: pipeline name (filename without extension)
   * value: Pipeline object
   */
  @NonCPS
  private def processPipelines() {
    def pipelines = [:]

    this.config.pipelines.each {
      Utils.log "Processing pipeline ${it.key}"

      ConfigObject settings = new ConfigObject(it.value)
      settings = ['basename': it.key] + settings

      String script = steps.loadResource("pipelines/${settings.basename}.pipeline", [this.path], true)
      if (script != null) {
        Pipeline pipeline = new Pipeline(it.key, settings, script, this)
        pipelines << ["${pipeline.name}": pipeline]
      }
    }

    return pipelines
  }

}
