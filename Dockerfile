FROM jenkins/jenkins:2.414.2-lts-jdk11

# metadata labels
LABEL \
    org.opencontainers.image.vendor="Sbit.io Tomorrow Consulting" \
    org.opencontainers.image.title="Official Keynes Docker image" \
    org.opencontainers.image.description="Jenkins preconfigured with Keynes" \
    org.opencontainers.image.url="https://gitlab.com/sbitio/keynes" \
    org.opencontainers.image.source="https://gitlab.com/sbitio/keynes" \
    org.opencontainers.image.licenses="MIT"

# Switch to root user to install system packages (plugin dependencies).
USER root
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update -qq \
 && echo "locales locales/default_environment_locale select $LOCALE" | debconf-set-selections \
 && echo "locales locales/locales_to_be_generated select $LOCALE $LOCALE_CHARSET" | debconf-set-selections \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get install -yqq -o=Dpkg::Use-Pty=0 --no-install-recommends \
      python3=3.* \
      pipx=1.* \
 && rm -rf /var/lib/apt/lists/* \
 && apt-get clean -yqq \
 && PIPX_HOME=/opt/pipx \
    PIPX_BIN_DIR=/usr/local/bin \
    pipx install --pip-args="--no-cache-dir" --include-deps \
      ansible==4.9.* \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get remove -yqq -o=Dpkg::Use-Pty=0 \
      pipx \
 && apt-get autoremove -yqq -o=Dpkg::Use-Pty=0

# Switch back to jenkins user for the rest of the build.
USER jenkins

# Uncomment to test ansible setup
#RUN ls -lah /usr/local/bin \
# && ansible-galaxy install ansistrano.deploy \
# && ansible -i localhost, -c local all -m ping

# Install plugins.
COPY docker/plugins.txt /usr/share/jenkins/plugins.txt
RUN /bin/jenkins-plugin-cli --plugin-file /usr/share/jenkins/plugins.txt

# Disable the setup wizard as we will set up jenkins as code.
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

# Copy the config-as-code yaml files into the image.
ENV CASC_JENKINS_CONFIG /var/jenkins_home/casc_configs
RUN mkdir -p /var/jenkins_home/casc_configs
COPY docker/casc/*.yaml /var/jenkins_home/casc_configs/
