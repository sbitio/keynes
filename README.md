[[_TOC_]]
# Keynes

Jenkins Pipeline Shared Library to feed JobDSL.

See [example](./example/README.md) for a complete reference on features, setup
and usage.

## Requirements

Minimum requirements are:

* [Git](https://plugins.jenkins.io/git)
* [Job DSL](https://plugins.jenkins.io/job-dsl)
* [Pipeline](https://plugins.jenkins.io/workflow-aggregator)
* [Pipeline Utility Steps](https://plugins.jenkins.io/pipeline-utility-steps)
* [Folders](https://plugins.jenkins.io/cloudbees-folder)
* [Config File Provider](https://plugins.jenkins.io/config-file-provider) (Optional)

Additionally, the provided pipelines require at least:

* [Ansible](https://plugins.jenkins.io/ansible)
* [Lockable Resources](https://plugins.jenkins.io/lockable-resources)
* [SSH Agent](https://plugins.jenkins.io/ssh-agent)
* [Workspace Cleanup](https://plugins.jenkins.io/ws-cleanup)

For full features, the provided pipelines require:

* [Docker Pipeline](https://plugins.jenkins.io/docker-workflow/)
* [Gatling](https://plugins.jenkins.io/gatling/)
* [HTML Publisher](https://plugins.jenkins.io/htmlpublisher/)
* [Violation Comments to GitLab](https://plugins.jenkins.io/violation-comments-to-gitlab/)
* [Warnings Next Generation](https://plugins.jenkins.io/warnings-ng/)

Other recommended plugins:

* [AnsiColor](https://plugins.jenkins.io/ansicolor)

See [example](./example/README.md) and [docker/plugins.txt](./docker/plugins.txt)

## Docker

A pre-configured functional docker image is provided on registry.gitlab.com/sbitio/keynes:latest

It's based on [jenkins/jenkins:lts](https://hub.docker.com/r/jenkins/jenkins)
and sets up Jenkins to run a Keynes-based project by implementing the
[MANUAL_JENKINS_SETUP](./MANUAL_JENKINS_SETUP.md) instructions.

Notes on security:
- For convenience it creates by default a jenkins user `keynes` with pass
`keynes`. You can override this mounting your custom casc file for securityRealm
on `/var/jenkins_home/casc_configs/jenkins_securityRealm.yaml`. See
[docker/casc/jenkins_securityRealm.yaml](./docker/casc/jenkins_securityRealm.yaml)
- For simplicity we opted to disable JobDSL security,
so we don't recomend to use this image in a production environment
(patches welcome). You can override security config mounting your custom casc
file on `/var/jenkins_home/casc_configs/security.yaml`. See
[docker/casc/security.yaml](./docker/casc/security.yaml)

The image contains a seed job named `_seed`, linked to `/seed` in the container,
so you want to mount a local folder with your projects on that location.

### Runing the container

```bash
NAME=my-keynes
docker run --name $NAME \
  -p 8080:8080 \
  --volume $NAME:/var/jenkins_home \
  --mount type=bind,source="$(pwd)",target=/seed \
  registry.gitlab.com/sbitio/keynes:latest
```

### Runing the container for Keynes development

To ease development of Keynes without the need to push every change to
gitlab.com, a local copy of Keynes can be exposed to the docker image and loaded
with `filesystem_scm` plugin. We provide
[dev/keynes_library.yaml](./dev/keynes_library.yaml) to ease the process.

For this, create the container as this:

```bash
PATH_TO_KEYNES=/opt/sbitio/keynes
NAME=keynes-dev
docker run --name $NAME --rm \
  -p 8080:8080 \
  --volume $NAME:/var/jenkins_home \
  --mount type=bind,source="$(pwd)",target=/seed \
  --mount type=bind,source="$PATH_TO_KEYNES",target=/keynes \
  --mount type=bind,source="$PATH_TO_KEYNES/dev/keynes_library.yaml",target=/var/jenkins_home/casc_configs/keynes_library.yaml \
  registry.gitlab.com/sbitio/keynes:latest
```

From this point on, any change to the files in the Keynes clone are available
in Jenkins with no need to commit + push.

Note that volume makes casc and several things persist across restarts, so when
dealing with issues or weirdeness remember to remove the volume and retest:
`docker volume rm $NAME`

## Development

Development happens on [Gitlab](https://gitlab.com/sbitio/keynes).

Please log issues for any bug report, feature or support request.


## License

MIT License, see LICENSE file

## Contact

Please use the contact form on http://sbit.io
