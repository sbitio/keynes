[[_TOC_]]
# Manual setup of Jenkins to run a Keynes-based project

## 0. Install jenkins

...

## 1. Install dependencies

Install the plugins listed in the Keynes README or [../docker/plugins.txt](../docker/plugins.txt)

## 2. Declare global library

Go to `Jenkins > Manage Jenkins > Configure System > Global Pipeline Libraries`
and fill at least:

```
Name: keynes
Project Repository: https://gitlab.com/sbitio/keynes
```

## 3. Create the seed job

Go to `Jenkins > New item`. Create a job of type `Pipeline` and fill at least:

```
Definition: Pipeline script from SCM
SCM: Git
Repository URL: ...
```

## 4. Allow JobDSL to run

Default security policy forbids execution of JobDSL code. You want

to install and configure [Authorize Project](https://plugins.jenkins.io/authorize-project)
plugin.

Alternatively you can disable JobDSL security, at your own risk.

## 5. Run it

Run the seed job.
