---

- name: 'Declare required variables'
  hosts: 'localhost'
  vars:
    ansible_connection: 'local'
  tasks:
    - no_log: true
      tags:
        - 'always'
      set_fact:
        required_variables:
          # script
          - 'appenv'
          - 'dotenv_store_path'
          - 'project'
          # extra-vars.yml
          - 'deploy_to'
          - 'dotenv_defaults'
          - 'dotenv_files'

- import_playbook: 'check-variables.yml'

- name: "Check environment is valid"
  hosts: 'localhost'
  vars:
    ansible_connection: 'local'
    envs: "{{ vars[dotenv_defaults['envs_var']] }}"
  tasks:
    - fail:
        msg: "Unknown environment «{{ appenv }}». Valid environments are: {{ envs.keys()|join(', ') }}"
      when: envs[appenv] is not defined

- name: "Update .env for {{ project }} - {{ appenv }}"
  hosts: "{{ inventory_maps[envs[appenv]['target']]|default(envs[appenv]['target']) }}"
  vars:
    envs: "{{ vars[dotenv_defaults['envs_var']] }}"
    default_owner: "{{ dotenv_defaults['owner']|default('deploy') }}"
    default_group: "{{ dotenv_defaults['group']|default(default_owner) }}"
    default_mode: "{{ dotenv_defaults['mode']|default('0644') }}"
    ask_confirmation: yes
  tasks:
    - set_fact:
        env_files_map: "{{ env_files_map|default([]) +
          [{
            'path': item.path,
            'src': lookup('ansible.builtin.first_found', _src_file_paths, skip=True),
            'dst': deploy_to + '/shared/' + item.path,
            'owner': item.owner|default(default_owner),
            'group': item.group|default(default_group),
            'mode': item.mode|default(default_mode),
          }]
        }}"
      vars:
        _src_file_paths:
          - "{{ dotenv_store_path + '/' + appenv + '/' + item.path }}"
          - "{{ dotenv_store_path + '/_default/' + item.path }}"
      loop: "{{ dotenv_files }}"
      loop_control:
        label: "{{ item.path }}"
      delegate_to: 'localhost'

    - fail:
        msg: "File {{ item.path }} not found in {{ dotenv_store_path }}. Aborting."
      when: not item.src
      loop: "{{ env_files_map }}"
      loop_control:
        label: "{{ item.path }}"
      delegate_to: 'localhost'

    - name: "diff -u {{ item.src }} {{ item.dst }}"
      copy:
        src: "{{ item.src }}"
        dest: "{{ item.dst }}"
        owner: "{{ item.owner }}"
        group: "{{ item.group }}"
        mode: "{{ item.mode }}"
      check_mode: yes
      diff: yes
      loop: "{{ env_files_map }}"
      loop_control:
        label: "{{ item.src }}"
      run_once: yes # Only checks one target
      register: 'diff_files'

    - name: "Confirm"
      pause:
        prompt: "Press ENTER to continue or «CTRL+C A ENTER» to abort"
      when: diff_files.changed and ask_confirmation|bool
      delegate_to: 'localhost'

    - debug:
        msg: "No changes. Aborting."
      when : not diff_files.changed

    - meta: end_play
      when: not diff_files.changed

    - name: "Copy environment file {{ item.src }}"
      copy:
        src: "{{ item.src }}"
        dest: "{{ item.dst }}"
        owner: "{{ item.owner }}"
        group: "{{ item.group }}"
        mode: "{{ item.mode }}"
      # Reduce noise
      diff: no
      loop: "{{ env_files_map }}"
      loop_control:
        label: "{{ item.src }}"
