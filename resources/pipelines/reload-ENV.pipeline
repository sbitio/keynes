#!groovy
@Library('keynes') _

/* Job vars */
def LOCK = env.JOB_NAME
Integer TIMEOUT = CONFIG.timeout
String TIMER = CONFIG.timer
def UPSTREAM = CONFIG.upstream
if (UPSTREAM instanceof String) {
  UPSTREAM = [
    upstreamProjects: CONFIG.upstream,
    threshold: "SUCCESS",
  ]
}
String ENVIRONMENT
def ENVIRONMENTS = VARS.environments.findAll{it.value != false}

if ( !params.RECONFIGURE_PIPELINE ) {
  /* Runtime vars */
  ENVIRONMENT = params.environment
  RELOAD_DB = getBoolean(params.reload_db)
  RELOAD_FILES = getBoolean(params.reload_files)
  DEPLOY = getBoolean(params.deploy)

  /* Populate job vars */
  // Don't lock the environment. The subjobs are responsible of managing the correct locks.
  //LOCK = "${CONFIG.name}-${ENVIRONMENT}"
}

pipeline {
  agent any
  options {
    skipDefaultCheckout()
    lock(LOCK)
    timeout(time: TIMEOUT, unit: 'MINUTES')
  }
  triggers {
    cron(TIMER)
    upstream(
      upstreamProjects: UPSTREAM.upstreamProjects,
      threshold: UPSTREAM.threshold,
    )
  }
  stages {
    stage('init') {
      steps {
        script {
          currentBuild.description = "Reload ${ENVIRONMENT}: ${currentBuild.description ?: '-'}"
        }
      }
    }
    stage('reload') {
      parallel {
        stage('database') {
          steps {
            script {
              def job_params = [
                string(name: 'environment', value: ENVIRONMENT)
              ]
              VARS.job_reload_db.params.each {
                job_params << string(name: it.key, value: it.value)
              }
              def buildStatus = build(
                job: VARS.job_reload_db.name,
                propagate: VARS.job_reload_db.propagate,
                parameters: job_params,
              )
              if ((!VARS.job_reload_db.propagate) && (buildStatus.result != 'SUCCESS')) {
                currentBuild.result = 'UNSTABLE'
              }
            }
          }
          when {
            expression {
              return RELOAD_DB
            }
          }
        }
        stage('files') {
          steps {
            script {
              def job_params = [
                string(name: 'environment', value: ENVIRONMENT)
              ]
              VARS.job_reload_files.params.each {
                job_params << string(name: it.key, value: it.value)
              }
              def buildStatus = build(
                job: VARS.job_reload_files.name,
                propagate: VARS.job_reload_files.propagate,
                parameters: job_params,
              )
              if ((!VARS.job_reload_files.propagate) && (buildStatus.result != 'SUCCESS')) {
                currentBuild.result = 'UNSTABLE'
              }
            }
          }
          when {
            expression {
              return RELOAD_FILES
            }
          }
        }
      }
    }
    stage('deploy') {
      steps {
        script {
          def buildStatus = build(
            job: VARS.job_deploy.name,
            propagate: VARS.job_deploy.propagate,
            parameters: [
              string(name: 'environment', value: ENVIRONMENT)
            ]
          )
          if ((!VARS.job_deploy.propagate) && (buildStatus.result != 'SUCCESS')) {
            currentBuild.result = 'UNSTABLE'
          }
        }
      }
      when {
        expression {
          return DEPLOY
        }
      }
    }
  }
  post {
    cleanup {
      script {
        if (params.RECONFIGURE_PIPELINE || CONFIG.cleanWS) {
          cleanWs()
        }
      }
    }
  }
}

// vi:syntax=groovy
