#!groovy
@Library('keynes') _

/* Job vars */
String LOCK = env.JOB_NAME
Integer TIMEOUT = CONFIG.timeout
String TIMER = CONFIG.timer
def UPSTREAM = CONFIG.upstream
if (UPSTREAM instanceof String) {
  UPSTREAM = [
    upstreamProjects: CONFIG.upstream,
    threshold: "SUCCESS",
  ]
}
def ANSIBLE_EXTRA_VARS_LOCAL
String ENVIRONMENT
String TARGET

def ENVIRONMENTS = VARS.environments.findAll{it.value != false}

if ( !params.RECONFIGURE_PIPELINE ) {
  /* Runtime vars */
  ENVIRONMENT = params.environment
  if (!ENVIRONMENTS.containsKey(ENVIRONMENT)) {
    if (currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause')) {
      currentBuild.description = "Unknown environment: ${ENVIRONMENT}."
      currentBuild.result = 'ERROR'
      error(currentBuild.description)
    }
    else if (VARS.environment_default == '') {
      currentBuild.description = 'Default environment not defined.'
      currentBuild.result = 'ERROR'
      error(currentBuild.description)
    }
    else {
      ENVIRONMENT = VARS.environment_default
    }
  }
  TARGET = ENVIRONMENTS[ENVIRONMENT]['target']
  TARGET = CONFIG.inventory_maps[TARGET] ?: TARGET

  /* Project vars */
  ANSIBLE_EXTRA_VARS_LOCAL=[
    appenv: ENVIRONMENT,
    target: TARGET,
  ] + VARS.ansible + params.findAll{it.key != 'RECONFIGURE_PIPELINE'}

  ansible_reserved_words = ['environment', 'timeout',]
  ansible_reserved_words.each {
    ANSIBLE_EXTRA_VARS_LOCAL.remove(it)
  }

  /* Job vars */
  LOCK = "${CONFIG.name}-${ENVIRONMENT}__${env.JOB_NAME}"
}

pipeline {
  agent any
  options {
    skipDefaultCheckout()
    lock(LOCK)
    timeout(time: TIMEOUT, unit: 'MINUTES')
  }
  triggers {
    cron(TIMER)
    upstream(
      upstreamProjects: UPSTREAM.upstreamProjects,
      threshold: UPSTREAM.threshold,
    )
  }
  stages {
    stage('init') {
      steps {
        script {
          currentBuild.description = "Run on ${ENVIRONMENT} (${TARGET}): ${currentBuild.description ?: '-'}"
        }
      }
    }
    stage('run') {
      steps {
        runAnsiblePlaybook(CONFIG.ansible, VARS.playbook, ANSIBLE_EXTRA_VARS_LOCAL, [VARS.credentials_ansible] + VARS.credentials_ssh_agent, '', params.check_first)
      }
    }
  }
  post {
    cleanup {
      script {
        if (params.RECONFIGURE_PIPELINE || CONFIG.cleanWS) {
          cleanWs()
        }
      }
    }
  }
}

// vi:syntax=groovy
