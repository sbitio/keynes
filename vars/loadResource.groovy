#!groovy

/**
 * Search for a file in the provided paths and returns its content.
 *
 * Contents of first file found is returned, unless 'all' parameter is true.
 * In this case return an array with contents of all files found.
 *
 * The search order is:
 *  - provided paths
 *  - workspace
 *  - library resources
 *
 */
@NonCPS
def call(String name, ArrayList paths = [], required = false, all = false) {
  ArrayList result = []
  File file

  paths += env.Workspace
  paths.each { path ->
    file = new File("$path/$name")
    if (file.exists()) {
      result.push(file.text)
      if (!all) return
    }
  }

  if (all || result.isEmpty()) {
    try {
      result.push(libraryResource(name))
    }
    catch (AbortException) {
      if (!all && result.isEmpty()) {
        level = required ? 'ERROR' : 'INFO'
        echo "${level}: File ${name} not found in ${paths} neither in Keynes library resources."
      }
    }
  }

  return all ? result : (result.isEmpty() ? null : result.first())
}
