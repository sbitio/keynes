#!groovy

def call(Map qa) {
  sh "mkdir -p build"

  qa.tools.each { name, config ->
    if (config.enabled) {
      if (config.docker) {
        def docker_image = config.docker.image + ':' + (config.docker.tag ?: 'latest')
        def docker_params = ''
        config.docker.volumes.each { src, dst ->
          if (new File(src).isAbsolute()) {
            src_abs = src
          }
          else {
            src_abs = env.WORKSPACE + '/' + src
          }
          if (!fileExists(src_abs)) {
            new File(src_abs).mkdirs()
          }
          docker_params = docker_params + ' -v ' + src_abs + ':' + dst
        }
        if (config.docker.args) {
          docker_params = config.docker.args + ' ' + docker_params
        }
        docker.image(docker_image).inside(docker_params) {
          sh "${config.command}"
        }
      }
      else {
        sh "${config.command}"
      }
    }
  }
}
