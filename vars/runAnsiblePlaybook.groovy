#!groovy

def call(ansible, playbook, extraVars = [], credentialsId = '', extras = '', check_first = false) {
  // Any playbook may implement the check-variables facility.
  ansible.extra_files << 'ansible/playbooks/check-variables.yml'

  if (ansible.inventory instanceof String) {
    ansible.inventory = [ansible.inventory]
  }

  // List of files to provision.
  String requirements_base = 'ansible/playbooks/requirements.yml'
  String requirements = playbook.replace('.yml', '-requirements.yml')
  ArrayList files = [playbook, requirements_base, requirements, ansible.config] + ansible.inventory + ansible.extra_files

  // Extra vars from files.
  ansible.extra_vars.each { filename ->
    files << filename
    extras += " --extra-vars @${filename}"
  }

  provisionFiles(files)

  // Local extra vars.
  extraVars['workspace'] = env.WORKSPACE

  // Write extra vars to a unique file name.
  extraVarsFile = "ansible/extra-vars-${BUILD_TAG}.json"
  writeJSON file: extraVarsFile, json: extraVars
  extras += " --extra-vars @${extraVarsFile}"

  withEnv([
    "PROJECT_NAME=${env.JOB_NAME.substring(0, env.JOB_NAME.indexOf('/'))}",
    "ANSIBLE_CONFIG=${ansible.config}",
    "ANSIBLE_INVENTORY=" + ansible.inventory.collect{"$env.WORKSPACE/$it"}.join(','),
  ]) {
    sh "ansible-galaxy install -r ${requirements_base}"
    if (fileExists(env.WORKSPACE + '/' + requirements)) {
      sh "ansible-galaxy install -r ${requirements}"
    }

    if (credentialsId instanceof String) {
      credentialsId = [credentialsId]
    }

    if (check_first) {
      sshagent(credentials: credentialsId, ignoreMissing: true) {
        ansiblePlaybook(
          installation: ansible.installation,
          playbook: playbook,
          credentialsId: credentialsId.size() ? credentialsId[0] : '',
          colorized: true,
          extras: "${extras} ${ansible.extras} --check",
        )
      }
      input id: "proceed", message: "Do you want to apply changes?"
    }

    sshagent(credentials: credentialsId, ignoreMissing: true) {
      ansiblePlaybook(
        installation: ansible.installation,
        playbook: playbook,
        credentialsId: credentialsId.size() ? credentialsId[0] : '',
        colorized: true,
        extras: "${extras} ${ansible.extras}",
      )
    }
  }
}
