#!groovy

def call(String credentialsId, Map remote, String command) {
  withCredentials([sshUserPrivateKey(credentialsId: credentialsId, keyFileVariable: 'identity', passphraseVariable: 'passphrase', usernameVariable: 'userName')]) {
    remote.user = userName
    remote.identityFile = identity
    remote.passphrase = passphrase
    return sshCommand(remote: remote, command: command)
  }
}
